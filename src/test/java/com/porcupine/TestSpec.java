package com.porcupine;


import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import com.porcupine.domain.JsonParsingService;
import com.porcupine.domain.ParliamentService;
import com.porcupine.infrastructure.json.JsonParsingServiceImpl;
import com.porcupine.infrastructure.parliament.RESTParliamentService;
import com.porcupine.mock.ParliamentServiceMock;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import org.junit.jupiter.api.BeforeEach;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class TestSpec {

    private final int port = 10000 + new Random().nextInt(1000);
    protected FiniteDuration timeout = Duration.create(120, TimeUnit.SECONDS);
    protected JsonParsingService jsonParsingService = new JsonParsingServiceImpl();
    private Config config = ConfigFactory.load().withValue("app.domain", ConfigValueFactory.fromAnyRef("http://localhost:" + port));
    protected ActorSystem actorSystem = ActorSystem.create("SingleQueryTest", config);
    protected ParliamentService parliamentService = new RESTParliamentService(actorSystem, ActorMaterializer.create(actorSystem), "http://localhost:" + port);
    private ParliamentServiceMock parliamentServiceMock;

    @BeforeEach
    public void setUp() throws Exception {
        if (parliamentServiceMock == null) {
            parliamentServiceMock = new ParliamentServiceMock();
            parliamentServiceMock.bind(port);
        }
    }

}
