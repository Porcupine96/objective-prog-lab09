package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.Inbox;
import com.porcupine.TestSpec;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("The TheMostOfQueryActor ")
class TheMostOfQueryActorTest extends TestSpec {

    @Test
    @DisplayName("should return the parliament member who was the longest abroad")
    void shouldReturnTheLongestAbroad() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new TheMostOfQueryActor.Get(id -> "/trips/" + id, jsonParsingService::extractTotalTripLength, Object::toString));

        assertEquals("John Sparrow - 15", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should return the parliament member who had the most trips")
    void shouldReturnTheMostOfTrips() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new TheMostOfQueryActor.Get(id -> "/trips/" + id, jsonParsingService::extractNumberOfTrips, Object::toString));

        assertEquals("John Sparrow - 10", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should return the parliament member who had the most expensive trip")
    void shouldReturnTheMostExpensiveTrip() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new TheMostOfQueryActor.Get(id -> "/trips/" + id, jsonParsingService::extractMaxTripPrice, Object::toString));

        assertEquals("John Sparrow - 300.0", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should handle a scenario where the external service returns unexpected response")
    void shouldHandleUnexpectedExternalServiceResponses() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("unexpectedResponse");
        inbox.watch(actor);
        inbox.send(actor, new TheMostOfQueryActor.Get(id -> "/trips/" + id, jsonParsingService::extractTotalTripLength, Object::toString));

        assertEquals("Error, unhandled response from an external service!", inbox.receive(timeout));
    }

    private ActorRef createActor(String firstId) {
        List<String> ids = new ArrayList<>();
        ids.add(firstId);
        ids.add("validId2");
        return actorSystem.actorOf(TheMostOfQueryActor.props(actorSystem, parliamentService, ids));
    }

}