package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.Inbox;
import com.porcupine.TestSpec;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("The SingleQueryActor ")
class SingleQueryActorTest extends TestSpec {

    @Test
    @DisplayName("should return the sum of expenses for the given member's name")
    void shouldReturnExpenseSum() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new SingleQueryActor.GetResource<>("validKey", id -> "/expenses/" + id, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("300.0", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should handle a scenario where the MP does not exist")
    void shouldHandleNonExistingMP() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new SingleQueryActor.GetResource<>("invalidKey", id -> "/expenses/" + id, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("Could not find the given member: invalidKey", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should handle a scenario where the external service returns unexpected response")
    void shouldHandleUnexpectedExternalServiceResponses() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("unexpectedResponse");
        inbox.watch(actor);
        inbox.send(actor, new SingleQueryActor.GetResource<>("validKey", id -> "/expenses/" + id, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("Error, unhandled response from an external service!", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should return the sum of expenses in a single category")
    void shouldReturnExpenseSumInACategory() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new SingleQueryActor.GetResource<>("validKey", id -> "/expenses/" + id, json -> jsonParsingService.extractExpensesInCategory("title-1", json), Object::toString));

        assertEquals("200.0", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should be able to handle a scenario where the category name is invalid")
    void shouldHandleInvalidName() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new SingleQueryActor.GetResource<>("validKey", id -> "/expenses/" + id, json -> jsonParsingService.extractExpensesInCategory("invalidCategory", json), Object::toString));

        assertEquals("Invalid category name!", inbox.receive(timeout));
    }

    private ActorRef createActor(String membersId) {
        HashMap<String, String> nameToId = new HashMap<>();
        nameToId.put("validKey", membersId);
        return actorSystem.actorOf(SingleQueryActor.props(actorSystem, parliamentService, nameToId));
    }

}