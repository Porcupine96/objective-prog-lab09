package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.Inbox;
import com.porcupine.TestSpec;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({"unchecked", "ConstantConditions"})
@DisplayName("The PredicateQueryActor ")
class PredicateQueryActorTest extends TestSpec {

    @Test
    @DisplayName("should return all members who visited Italy")
    void shouldReturnAllMembersWhoVisitedItaly() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor();
        inbox.watch(actor);
        inbox.send(actor, new PredicateQueryActor.AllWho<>(id -> "/trips/" + id, visitedCountries -> visitedCountries.contains("Italy"), jsonParsingService::extractVisitedCountries, Object::toString));

        assertEquals("[Jack Sparrow]", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should return an empty list of members if none of them met the predicate")
    void shouldReturnAnEmptyList() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor();
        inbox.watch(actor);
        inbox.send(actor, new PredicateQueryActor.AllWho<>(id -> "/trips/" + id, visitedCountries -> visitedCountries.contains("China"), jsonParsingService::extractVisitedCountries, Object::toString));

        assertEquals("[]", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should handle a scenario where the external service returns unexpected response")
    void shouldHandleUnexpectedExternalServiceResponses() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor();
        inbox.watch(actor);
        inbox.send(actor, new PredicateQueryActor.AllWho<>(id -> "/trips/unexpectedResponse", visitedCountries -> visitedCountries.contains("China"), jsonParsingService::extractVisitedCountries, Object::toString));

        assertEquals("Error, unhandled response from an external service", inbox.receive(timeout));
    }

    private ActorRef createActor() {
        List<String> ids = new ArrayList<>();
        ids.add("validId");
        return actorSystem.actorOf(PredicateQueryActor.props(actorSystem, parliamentService, ids));
    }

}