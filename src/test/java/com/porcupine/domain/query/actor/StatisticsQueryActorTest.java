package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.Inbox;
import com.porcupine.TestSpec;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("The StatisticsQueryActor ")
class StatisticsQueryActorTest extends TestSpec {

    private final BiFunction<Double, Double, Double> forEach = (x, acc) -> acc + x;
    private final Function<Double, Double> finalizer = x -> x / 2;

    @Test
    @DisplayName("should return the average of members expenses")
    void shouldReturnAverage() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("validId");
        inbox.watch(actor);
        inbox.send(actor, new StatisticsQueryActor.GetStatistics<>(0d, id -> "/expenses/" + id, forEach, finalizer, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("350.0", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should handle a scenario where the external service returns unexpected response")
    void shouldHandleUnexpectedExternalServiceResponses() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = createActor("unexpectedResponse");
        inbox.watch(actor);
        inbox.send(actor, new StatisticsQueryActor.GetStatistics<>(0d, id -> "/expenses/" + id, forEach, finalizer, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("Error, unhandled response from an external service!", inbox.receive(timeout));
    }

    @Test
    @DisplayName("should return 0 if the list of parliament members is empty")
    void shouldReturnZero() throws Exception {
        Inbox inbox = Inbox.create(actorSystem);
        ActorRef actor = actorSystem.actorOf(StatisticsQueryActor.props(actorSystem, parliamentService, new ArrayList<>()));
        inbox.watch(actor);
        inbox.send(actor, new StatisticsQueryActor.GetStatistics<>(0d, id -> "/expenses/" + id, forEach, finalizer, jsonParsingService::extractExpensesSum, Object::toString));

        assertEquals("0", inbox.receive(timeout));
    }

    private ActorRef createActor(String firstId) {
        List<String> ids = new ArrayList<>();
        ids.add(firstId);
        ids.add("validId2");
        return actorSystem.actorOf(StatisticsQueryActor.props(actorSystem, parliamentService, ids));
    }



}