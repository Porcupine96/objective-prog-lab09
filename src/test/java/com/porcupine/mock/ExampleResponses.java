package com.porcupine.mock;

class ExampleResponses {


    static String membersExpenses(int expense) {
        return "{\n" +
                "\"layers\": {\n" +
                "    \"wydatki\": {\n" +
                "      \"punkty\": [\n" +
                "        {\n" +
                "          \"tytul\": \"title-1\",\n" +
                "          \"numer\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"tytul\": \"title-2\",\n" +
                "          \"numer\": \"2\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"roczniki\": [\n" +
                "        {\n" +
                "          \"pola\": [\n" +
                "            \"" + expense + "\",\n" +
                "            \"0\"\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"pola\": [\n" +
                "            \"100\",\n" +
                "            \"100\"\n" +
                "          ]\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  } \n" +
                "}";
    }

    static String memberTrips(String name, int day, String tripPrice) {
        return "{\n" +
                "  \"data\": {\n" +
                "    \"poslowie.imie_pierwsze\": \"" + name + "\",\n" +
                "    \"poslowie.nazwisko\": \"Sparrow\",\n" +
                "    \"poslowie.liczba_wyjazdow\": \"10\"\n" +
                "  },\n" +
                "  \"layers\": {\n" +
                "    \"wyjazdy\": [\n" +
                "      {\n" +
                "        \"kraj\": \"Italy\",\n" +
                "        \"liczba_dni\": \"3\",\n" +
                "        \"od\": \"2013-06-16\",\n" +
                "        \"do\": \"2013-06-" + day + "\",\n" +
                "        \"koszt_suma\": \"" + tripPrice + "\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"kraj\": \"England\",\n" +
                "        \"liczba_dni\": \"2\",\n" +
                "        \"od\": \"2012-05-30\",\n" +
                "        \"do\": \"2012-05-31\",\n" +
                "        \"koszt_suma\": \"200.00\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
    }

}
