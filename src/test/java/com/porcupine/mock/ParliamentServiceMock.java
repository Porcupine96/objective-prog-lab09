package com.porcupine.mock;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;

import static akka.http.javadsl.server.PathMatchers.segment;

public class ParliamentServiceMock extends AllDirectives {

    private final ActorSystem system = ActorSystem.create("parliament-routes");
    private final Http http = Http.get(system);
    private final ActorMaterializer materializer = ActorMaterializer.create(system);

    private final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = createRoute().flow(system, materializer);

    private Route createRoute() {
        return
                path(segment("expenses").slash("validId"), () -> get(() -> complete(StatusCodes.OK, ExampleResponses.membersExpenses(100))))
                        .orElse(
                                path(segment("expenses").slash("validId2"), () -> get(() -> complete(StatusCodes.OK, ExampleResponses.membersExpenses(200))))
                        )
                        .orElse(
                                path(segment("expenses").slash("unexpectedResponse"), () -> get(() -> complete(StatusCodes.NOT_FOUND)))
                        )
                        .orElse(
                                path(segment("trips").slash("validId"), () -> get(() -> complete(StatusCodes.OK, ExampleResponses.memberTrips("Jack", 20, "100.00"))))
                        )
                        .orElse(
                                path(segment("trips").slash("validId2"), () -> get(() -> complete(StatusCodes.OK, ExampleResponses.memberTrips("John", 30, "300.00"))))
                        )
                        .orElse(
                        path(segment("trips").slash("unexpectedResponse"), () -> get(() -> complete(StatusCodes.NOT_FOUND)))
                );
    }

    public void bind(int port) {
        http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", port), materializer);
    }

}
