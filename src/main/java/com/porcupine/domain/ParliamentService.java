package com.porcupine.domain;

import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public interface ParliamentService {

    <T> CompletionStage<T> consumeRequest(String url, Function<String, T> consumer);

}