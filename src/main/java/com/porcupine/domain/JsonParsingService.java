package com.porcupine.domain;

import com.porcupine.domain.query.actor.TheMostOfQueryActor;
import com.porcupine.infrastructure.json.JsonServiceResponse;
import org.json.JSONException;

import java.util.List;
import java.util.Map;

public interface JsonParsingService {

    int extractPageNumber(String jsonString) throws JSONException;

    Map<String, String> extractAMapOfNamesToId(String jsonString) throws JSONException;

    double extractExpensesSum(String jsonString) throws JSONException;

    double extractExpensesInCategory(String category, String jsonString) throws JSONException;

    TheMostOfQueryActor.TheMostOfActorResponse<Integer> extractNumberOfTrips(String jsonString) throws JSONException;

    TheMostOfQueryActor.TheMostOfActorResponse<Double> extractMaxTripPrice(String jsonString) throws JSONException;

    TheMostOfQueryActor.TheMostOfActorResponse<Integer> extractTotalTripLength(String jsonString) throws JSONException;

    JsonServiceResponse<List<String>> extractVisitedCountries(String jsonString) throws JSONException;

}
