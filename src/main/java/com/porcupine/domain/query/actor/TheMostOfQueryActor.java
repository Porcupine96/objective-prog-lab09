package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;
import com.porcupine.domain.ParliamentService;
import org.json.JSONException;

import java.util.List;
import java.util.function.Function;

import static akka.pattern.PatternsCS.pipe;

@SuppressWarnings("unchecked")
public class TheMostOfQueryActor<R extends Comparable> extends UntypedActor {

    private final ActorSystem system;

    private final ParliamentService parliamentService;
    private final List<String> ids;

    private TheMostOfActorResponse currentBest;
    private Function<String, String> responseWrapper;
    private int pendingRequests;

    private TheMostOfQueryActor(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        this.system = system;
        this.parliamentService = parliamentService;
        this.ids = ids;
    }

    static Props props(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        return Props.create(
                TheMostOfQueryActor.class,
                (Creator<TheMostOfQueryActor>) () -> new TheMostOfQueryActor<>(system, parliamentService, ids)
        ).withDispatcher("the-most-of-dispatcher");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof TheMostOfQueryActor.Get) get((Get) message);
        else if (message instanceof TheMostOfQueryActor.SingleComplete) processCompleted((SingleComplete<R>) message);
        else unhandled("Unhandled message send to TheMostOfQueryActor" + message);
    }

    private void get(Get command) {
        this.pendingRequests = ids.size();
        this.responseWrapper = command.responseWrapper;
        ids.forEach(id ->
                pipe(
                        parliamentService.consumeRequest(command.urlBuilder.apply(id), json -> new SingleComplete(json, command.extractor)),
                        system.dispatcher()
                ).to(self(), sender()));
    }

    private void processCompleted(SingleComplete<R> command) {
        try {
            TheMostOfActorResponse candidate = command.extractor.apply(command.json);
            if (currentBest == null || candidate.resource.compareTo(currentBest.resource) > 0) currentBest = candidate;
            if (--pendingRequests == 0)
                sender().tell(responseWrapper.apply(currentBest.toString()), ActorRef.noSender());
        } catch (JSONException ex) {
            sender().tell("Error, unhandled response from an external service!", ActorRef.noSender());
        }
    }

    /* public commands */

    static class Get {
        final Function<String, String> urlBuilder;
        final Function<String, TheMostOfActorResponse<? extends Comparable>> extractor;
        final Function<String, String> responseWrapper;

        Get(Function<String, String> urlBuilder, Function<String, TheMostOfActorResponse<?>> extractor, Function<String, String> responseWrapper) {
            this.urlBuilder = urlBuilder;
            this.extractor = extractor;
            this.responseWrapper = responseWrapper;
        }
    }

    /* internal commands */

    private static class SingleComplete<T extends Comparable> {
        final String json;
        final Function<String, TheMostOfActorResponse<T>> extractor;

        SingleComplete(String json, Function<String, TheMostOfActorResponse<T>> extractor) {
            this.json = json;
            this.extractor = extractor;
        }
    }

    public static class TheMostOfActorResponse<T extends Comparable> {
        final String firstName;
        final String lastName;
        final T resource;

        public TheMostOfActorResponse(String firstName, String lastName, T resource) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.resource = resource;
        }

        @Override
        public String toString() {
            return firstName + " " + lastName + " - " + resource;
        }
    }

}
