package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import com.porcupine.domain.ParliamentService;
import org.json.JSONException;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static akka.pattern.PatternsCS.pipe;


@SuppressWarnings("unchecked")
public class StatisticsQueryActor<T, R, S> extends UntypedActor {

    private final ActorSystem system;

    private final ParliamentService parliamentService;
    private final List<String> ids;

    private BiFunction<T, R, R> forEach;
    private Function<R, S> finalizer;
    private Function<String, T> extractor;
    private Function<String, String> responseWrapper;

    private R currentResult;
    private int pendingRequests;

    private StatisticsQueryActor(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        this.system = system;
        this.parliamentService = parliamentService;
        this.ids = ids;
    }

    static Props props(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        return Props.create(
                StatisticsQueryActor.class,
                (Creator<StatisticsQueryActor>) () -> new StatisticsQueryActor(system, parliamentService, ids)
        ).withDispatcher("statistics-dispatcher");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof GetStatistics) getStatistics((GetStatistics<T, R, S>) message);
        else if (message instanceof SingleComplete) processCompleted((SingleComplete) message);
        else unhandled("Unhandled message send to StatisticsQueryActor" + message);

    }

    private void getStatistics(GetStatistics<T, R, S> command) {
        this.currentResult = command.initialValue;
        this.forEach = command.forEach;
        this.finalizer = command.finalizer;
        this.extractor = command.extractor;
        this.responseWrapper = command.responseWrapper;
        this.pendingRequests = ids.size();
        if (pendingRequests == 0) sender().tell(responseWrapper.apply("0"), ActorRef.noSender());
        ids.forEach(id ->
                pipe(
                        parliamentService.consumeRequest(command.urlBuilder.apply(id), SingleComplete::new),
                        system.dispatcher()
                ).to(self(), sender())
        );
    }

    private void processCompleted(SingleComplete command) {
        try {
            currentResult = forEach.apply(extractor.apply(command.json), currentResult);
        if (--pendingRequests == 0) sender().tell(responseWrapper.apply(finalizer.apply(currentResult).toString()), ActorRef.noSender());
        } catch (JSONException ex) {
            sender().tell("Error, unhandled response from an external service!", ActorRef.noSender());
        }
    }

    /* internal commands */

    private static class SingleComplete {
        final String json;

        SingleComplete(String json) {
            this.json = json;
        }
    }

    /* public interface */

    static class GetStatistics<T, R, S> {
        final R initialValue;
        final Function<String, String> urlBuilder;
        final BiFunction<T, R, R> forEach;
        final Function<R, S> finalizer;
        final Function<String, T> extractor;
        final Function<String, String> responseWrapper;

        GetStatistics(R initialValue, Function<String, String> urlBuilder, BiFunction<T, R, R> forEach, Function<R, S> finalizer, Function<String, T> extractor, Function<String, String> responseWrapper) {
            this.initialValue = initialValue;
            this.urlBuilder = urlBuilder;
            this.forEach = forEach;
            this.finalizer = finalizer;
            this.extractor = extractor;
            this.responseWrapper = responseWrapper;
        }
    }

}
