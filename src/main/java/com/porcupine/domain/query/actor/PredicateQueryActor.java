package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import com.porcupine.domain.ParliamentService;
import com.porcupine.infrastructure.json.JsonServiceResponse;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static akka.pattern.PatternsCS.pipe;

@SuppressWarnings("unchecked")
public class PredicateQueryActor<T> extends UntypedActor {

    private final ActorSystem system;

    private final ParliamentService parliamentService;
    private final List<String> ids;
    private Function<String, String> responseWrapper;

    private int pendingRequests;
    private List<String> currentWhoMet = new ArrayList<>();

    private PredicateQueryActor(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        this.system = system;
        this.parliamentService = parliamentService;
        this.ids = ids;
    }

    static Props props(ActorSystem system, ParliamentService parliamentService, List<String> ids) {
        return Props.create(
                PredicateQueryActor.class,
                (Creator<PredicateQueryActor>) () -> new PredicateQueryActor<>(system, parliamentService, ids)
        ).withDispatcher("predicate-dispatcher");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof AllWho) allWho((AllWho<T>) message);
        else if (message instanceof PredicateQueryActor.SingleComplete) singleComplete((SingleComplete<T>) message);
        else unhandled("Unhandled message send to PredicateQueryActor" + message);
    }

    private void allWho(AllWho<T> command) {
        this.pendingRequests = ids.size();
        this.responseWrapper = command.responseWrapper;
        ids.forEach(id ->
                pipe(
                        parliamentService.consumeRequest(command.urlBuilder.apply(id), json -> new PredicateQueryActor.SingleComplete<>(json, command.predicate, command.extractor)),
                        system.dispatcher()
                ).to(self(), sender()));
    }

    private void singleComplete(PredicateQueryActor.SingleComplete<T> command) {
        try {
            JsonServiceResponse<T> response = command.extractor.apply(command.json);
            if (command.predicate.test(response.resource))
                currentWhoMet.add(response.firstName + " " + response.lastName);
            if (--pendingRequests == 0)
                sender().tell(responseWrapper.apply(currentWhoMet.toString()), ActorRef.noSender());
        } catch (JSONException ex) {
            sender().tell("Error, unhandled response from an external service", ActorRef.noSender());
        }
    }


    /* public commands */

    static class AllWho<T> {
        final Function<String, String> urlBuilder;
        final Predicate<T> predicate;
        final Function<String, JsonServiceResponse<T>> extractor;
        final Function<String, String> responseWrapper;

        AllWho(Function<String, String> urlBuilder, Predicate<T> predicate, Function<String, JsonServiceResponse<T>> extractor, Function<String, String> responseWrapper) {
            this.urlBuilder = urlBuilder;
            this.predicate = predicate;
            this.extractor = extractor;
            this.responseWrapper = responseWrapper;
        }
    }

    /* internal commands */

    private static class SingleComplete<T> {
        final String json;
        final Predicate<T> predicate;
        final Function<String, JsonServiceResponse<T>> extractor;

        SingleComplete(String json, Predicate<T> predicate, Function<String, JsonServiceResponse<T>> extractor) {
            this.json = json;
            this.predicate = predicate;
            this.extractor = extractor;
        }
    }

}
