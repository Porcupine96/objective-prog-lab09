package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActorWithStash;
import akka.japi.Creator;
import akka.japi.Procedure;
import com.porcupine.domain.JsonParsingService;
import com.porcupine.domain.ParliamentService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static akka.pattern.PatternsCS.pipe;


@SuppressWarnings("unchecked")
public class ParliamentQueryActor extends UntypedActorWithStash {

    private final ActorSystem system;

    private final ParliamentService parliamentService;
    private final String allMembersUrl;
    private JsonParsingService jsonParsingService;
    private Function<String, String> tripsUrlBuilder;
    private Function<String, String> expensesUrlBuilder;
    private Map<String, String> nameToId;
    private ActorRef requester;
    private int pendingRequests;

    private ParliamentQueryActor(ActorSystem system, String termOfOfficeUrl, String baseUrl, ParliamentService parliamentService, JsonParsingService jsonParsingService) {
        this.system = system;
        this.allMembersUrl = baseUrl + termOfOfficeUrl;
        this.tripsUrlBuilder = id -> baseUrl + id + "?layers[]=wyjazdy";
        this.expensesUrlBuilder = id -> baseUrl + id + "?layers[]=wydatki";
        this.parliamentService = parliamentService;
        this.jsonParsingService = jsonParsingService;
        this.nameToId = new HashMap<>();
    }

    public static Props props(ActorSystem system, ParliamentService parliamentService, String termOfOfficeUrl, String baseUrl, JsonParsingService jsonParsingService) {
        return Props.create(
                ParliamentQueryActor.class,
                (Creator<ParliamentQueryActor>) () -> new ParliamentQueryActor(system, termOfOfficeUrl, baseUrl, parliamentService, jsonParsingService)
        ).withDispatcher("main-dispatcher");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        waitingForInitialisation.apply(message);
    }

    private Procedure<Object> waitingForInitialisation = (Procedure<Object>) message -> {
        if (message instanceof PageCompleted) {
            PageCompleted pageResult = (PageCompleted) message;
            nameToId.putAll(jsonParsingService.extractAMapOfNamesToId(pageResult.json));
            int totalPageCount = jsonParsingService.extractPageNumber(pageResult.json);
            IntStream.rangeClosed(2, totalPageCount).forEach(this::sendRequest);
            getContext().become(new GettingMembers(totalPageCount - 1));
        } else {
            stash();
            sendRequest(1);
        }
    };

    private class GettingMembers<T> implements Procedure<T> {

        private int pendingPages;

        GettingMembers(int pendingPages) {
            this.pendingPages = pendingPages;
        }

        @Override
        public void apply(Object message) throws Exception {
            if (message instanceof PageCompleted) {
                PageCompleted pageResult = (PageCompleted) message;
                nameToId.putAll(jsonParsingService.extractAMapOfNamesToId(pageResult.json));
                if (--pendingPages == 0) {
                    unstashAll();
                    getContext().become(initialised);
                } else getContext().become(new GettingMembers<>(pendingPages));
            } else stash();
        }
    }

    private Procedure<Object> initialised = (message) -> {
        if (message instanceof MultipleQueries) multipleQueries((MultipleQueries) message);
        else if (message instanceof TheMostOftenAbroad) theMostOftenAbroad((TheMostOftenAbroad) message);
        else if (message instanceof TheMostExpensiveTrip) theMostExpensiveTrip((TheMostExpensiveTrip) message);
        else if (message instanceof TheLongestAbroad) theLongestAbroad((TheLongestAbroad) message);
        else if (message instanceof VisitedACountry) thoseWhoVisitedCertainCountry((VisitedACountry) message);
        else if (message instanceof AverageExpenses) getAverageExpenses((AverageExpenses) message);
        else if (message instanceof TotalExpenses) getTotalExpenses((TotalExpenses) message);
        else if (message instanceof ExpensesInCategory) getExpensesInCategory((ExpensesInCategory) message);
        else if (message instanceof String) tryToResponse((String) message);
        else unhandled("Unhandled message send to ParliamentQueryActor" + message);
    };

    private void multipleQueries(MultipleQueries command) {
        requester = sender();
        pendingRequests = command.commands.size();
        if (pendingRequests == 0) {
            System.out.println("Empty query!");
            requester.tell("done", ActorRef.noSender());
        }
        command.commands.forEach(c -> self().tell(c, self()));
    }

    private void tryToResponse(String response) {
        System.out.println(response);
        if (--pendingRequests == 0) requester.tell("done", ActorRef.noSender());
    }

    private void theMostOftenAbroad(TheMostOftenAbroad command) {
        delegate(new TheMostOfQueryActor.Get(tripsUrlBuilder, jsonParsingService::extractNumberOfTrips, command.responseWrapper));
    }

    private void theMostExpensiveTrip(TheMostExpensiveTrip command) {
        delegate(new TheMostOfQueryActor.Get(tripsUrlBuilder, jsonParsingService::extractMaxTripPrice, command.responseWrapper));
    }

    private void theLongestAbroad(TheLongestAbroad command) {
        delegate(new TheMostOfQueryActor.Get(tripsUrlBuilder, jsonParsingService::extractTotalTripLength, command.responseWrapper));
    }

    private void thoseWhoVisitedCertainCountry(VisitedACountry command) {
        Predicate<List<String>> visitedACountry = visitedCountries -> visitedCountries.contains(command.countryName);
        delegate(new PredicateQueryActor.AllWho<>(tripsUrlBuilder, visitedACountry, jsonParsingService::extractVisitedCountries, command.responseWrapper));
    }

    private void getAverageExpenses(AverageExpenses command) {
        BiFunction<Double, Double, Double> forEach = (x, acc) -> acc + x;
        Function<Double, Double> finalizer = x -> x / nameToId.size();
        delegate(new StatisticsQueryActor.GetStatistics<>(0d, expensesUrlBuilder, forEach, finalizer, jsonParsingService::extractExpensesSum, command.responseWrapper));
    }

    private void getTotalExpenses(TotalExpenses command) {
        delegate(new SingleQueryActor.GetResource<>(command.names(), expensesUrlBuilder, jsonParsingService::extractExpensesSum, command.responseWrapper));
    }

    private void getExpensesInCategory(ExpensesInCategory command) {
        String key = command.names();
        Function<String, Double> extractor = json -> jsonParsingService.extractExpensesInCategory(command.getCategoryName(), json);
        delegate(new SingleQueryActor.GetResource<>(key, expensesUrlBuilder, extractor, command.responseWrapper));
    }

    private void sendRequest(int page) {
        pipe(
                parliamentService.consumeRequest(allMembersUrl + "&page=" + page, json -> new PageCompleted(json, false)),
                system.dispatcher()
        ).to(self(), sender());
    }

    private void delegate(TheMostOfQueryActor.Get command) {
        createTheMostOfActor().tell(command, self());
    }

    private void delegate(PredicateQueryActor.AllWho command) {
        createPredicateQueryActor().tell(command, self());
    }

    private void delegate(StatisticsQueryActor.GetStatistics command) {
        createStatisticsQueryActor().tell(command, self());
    }

    private void delegate(SingleQueryActor.GetResource command) {
        createSingleQueryActor().tell(command, self());
    }

    private ActorRef createStatisticsQueryActor() {
        return system.actorOf(StatisticsQueryActor.props(system, parliamentService, new ArrayList<>(nameToId.values())));
    }

    private ActorRef createTheMostOfActor() {
        return system.actorOf(TheMostOfQueryActor.props(system, parliamentService, new ArrayList<>(nameToId.values())));
    }

    private ActorRef createPredicateQueryActor() {
        return system.actorOf(PredicateQueryActor.props(system, parliamentService, new ArrayList<>(nameToId.values())));
    }

    private ActorRef createSingleQueryActor() {
        return system.actorOf(SingleQueryActor.props(system, parliamentService, nameToId));
    }


    /* internal commands */

    private static class PageCompleted {
        final String json;
        final boolean generateNext;

        PageCompleted(String json, boolean generateNext) {
            this.json = json;
            this.generateNext = generateNext;
        }
    }

    /* public commands */

    public interface ParliamentCommand {}

    public static final class AverageExpenses implements ParliamentCommand {
        final Function<String, String> responseWrapper = r -> "Average expenses of all parliament members: " + r + " PLN";
    }

    public static final class TheMostOftenAbroad implements ParliamentCommand {
        final Function<String, String> responseWrapper = r -> "The member of parliament who was the most often abroad: " + r + " times";
    }

    public static final class TheMostExpensiveTrip implements ParliamentCommand {
        final Function<String, String> responseWrapper = r -> "The member of parliament who was on the most expensive trip: " + r + " PLN";
    }

    public static final class TheLongestAbroad implements ParliamentCommand {
        final Function<String, String> responseWrapper = r -> "The member of parliament who was the longest abroad: " + r + " days";
    }

    public static final class VisitedACountry implements ParliamentCommand {
        String countryName;
        final Function<String, String> responseWrapper = r -> "Parliament members who visited " + countryName + ": " + r;

        public VisitedACountry(String countryName) {this.countryName = countryName;}
    }

    public static final class TotalExpenses implements ParliamentCommand {
        private String firstName;
        private String lastName;
        final Function<String, String> responseWrapper = r -> "Total expenses of " + firstName + " " + lastName + ": " + r + " PLN";

        public TotalExpenses(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        String names() {
            return firstName + " " + lastName;
        }
    }

    public static final class ExpensesInCategory implements ParliamentCommand {
        private String firstName;
        private String lastName;
        private String categoryName;

        final Function<String, String> responseWrapper = r -> "Expenses in category \"" + categoryName + "\" of " + firstName + " " + lastName + ": " + r + " PLN";

        public ExpensesInCategory(String firstName, String lastName, String categoryName) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.categoryName = categoryName;
        }

        String names() {
            return firstName + " " + lastName;
        }

        String getCategoryName() { return categoryName; }
    }

    public static final class MultipleQueries {
        final List<ParliamentQueryActor.ParliamentCommand> commands;

        public MultipleQueries(List<ParliamentCommand> commands) {
            this.commands = commands;
        }
    }

}
