package com.porcupine.domain.query.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.dsl.Creators;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import com.porcupine.domain.ParliamentService;
import com.sun.javaws.exceptions.InvalidArgumentException;
import com.sun.tools.corba.se.idl.InvalidArgument;
import org.json.JSONException;

import java.util.Map;
import java.util.function.Function;

import static akka.pattern.PatternsCS.pipe;

@SuppressWarnings("unchecked")
public class SingleQueryActor<T> extends UntypedActor {

    private final ActorSystem system;
    private final ParliamentService parliamentService;
    private final Map<String, String> nameToId;
    private Function<String, T> extractor;
    private Function<String, String> responseWrapper;

    private SingleQueryActor(ActorSystem system, ParliamentService parliamentService, Map<String, String> nameToId) {
        this.system = system;
        this.parliamentService = parliamentService;
        this.nameToId = nameToId;
    }

    static Props props(ActorSystem system, ParliamentService parliamentService, Map<String, String> nameToId) {
        return Props.create(
                SingleQueryActor.class,
                (Creator<SingleQueryActor>) () -> new SingleQueryActor<>(system, parliamentService, nameToId)
        ).withDispatcher("single-dispatcher");
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof SingleQueryActor.GetResource) getResource((GetResource<T>) message);
        else if (message instanceof SingleComplete) processCompleted((SingleComplete) message);
        else unhandled("Unhandled message send to SingleQueryActor" + message);
    }

    private void getResource(GetResource<T> command) {
        this.extractor = command.extractor;
        this.responseWrapper = command.responseWrapper;
        String id = nameToId.get(command.key);
        if (id != null) {
            pipe(
                    parliamentService.consumeRequest(command.urlBuilder.apply(id), SingleComplete::new),
                    system.dispatcher()
            ).to(self(), sender());
        } else sender().tell("Could not find the given member: " + command.key, ActorRef.noSender());
    }

    private void processCompleted(SingleComplete command) {
        try {
            sender().tell(responseWrapper.apply(extractor.apply(command.json).toString()), ActorRef.noSender());
        } catch (JSONException ex) {
            sender().tell("Error, unhandled response from an external service!", ActorRef.noSender());
        } catch (IllegalArgumentException ex) {
            sender().tell("Invalid category name!", ActorRef.noSender());
        }
    }


    /* internal commands */

    private static class SingleComplete {
        final String json;

        SingleComplete(String json) {
            this.json = json;
        }
    }

    /* public commands */

    static class GetResource<T> {
        final String key;
        final Function<String, String> urlBuilder;
        final Function<String, T> extractor;
        final Function<String, String> responseWrapper;

        GetResource(String key, Function<String, String> urlBuilder, Function<String, T> extractor, Function<String, String> responseWrapper) {
            this.key = key;
            this.urlBuilder = urlBuilder;
            this.extractor = extractor;
            this.responseWrapper = responseWrapper;
        }
    }

}
