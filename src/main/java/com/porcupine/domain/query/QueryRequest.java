package com.porcupine.domain.query;

import com.porcupine.domain.query.actor.ParliamentQueryActor;

import java.util.List;

public class QueryRequest {

    private final String termOfOfficeUrl;
    private final List<ParliamentQueryActor.ParliamentCommand> commands;

    public QueryRequest(String termOfOfficeUrl, List<ParliamentQueryActor.ParliamentCommand> commands) {
        this.termOfOfficeUrl = termOfOfficeUrl;
        this.commands = commands;
    }

    public String getTermOfOfficeUrl() {
        return termOfOfficeUrl;
    }

    public List<ParliamentQueryActor.ParliamentCommand> getCommands() {
        return commands;
    }

}
