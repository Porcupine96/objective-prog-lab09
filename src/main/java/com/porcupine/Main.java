package com.porcupine;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.porcupine.domain.JsonParsingService;
import com.porcupine.domain.ParliamentService;
import com.porcupine.domain.query.QueryRequest;
import com.porcupine.domain.query.actor.ParliamentQueryActor;
import com.porcupine.infrastructure.json.JsonParsingServiceImpl;
import com.porcupine.infrastructure.parliament.RESTParliamentService;
import com.porcupine.utils.ArgumentParser;
import com.porcupine.utils.ArgumentResult;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.cli.ParseException;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main {

    public static void main(String[] args) throws TimeoutException {
        Config config = ConfigFactory.load();
        ActorSystem system = ActorSystem.create("AppActorSystem", config);
        try {
            ArgumentResult parseResult = ArgumentParser.parse(args);
            QueryRequest queryRequest = parseResult.toQueryRequest();

            Materializer materializer = ActorMaterializer.create(system);
            String domain = config.getString("app.domain");
            String baseUrl = config.getString("app.baseUrl");

            ParliamentService parliamentService = new RESTParliamentService(system, materializer, domain);
            JsonParsingService jsonParsingService = new JsonParsingServiceImpl();
            Inbox inbox = Inbox.create(system);

            long time = System.currentTimeMillis();
            ActorRef parliamentQueryActor = system.actorOf(ParliamentQueryActor.props(system, parliamentService, queryRequest.getTermOfOfficeUrl(), baseUrl, jsonParsingService));

            inbox.watch(parliamentQueryActor);
            inbox.send(parliamentQueryActor, new ParliamentQueryActor.MultipleQueries(queryRequest.getCommands()));
            inbox.receive(Duration.create(150, TimeUnit.SECONDS));

            System.out.println("Received after " + (System.currentTimeMillis() - time) / 1000d + " seconds");

        } catch (ParseException ex) {
            System.out.println("Unsupported command line argument, supported options are:\n" +
                    "-k, -termOfOffice [number]\n" +
                    "-e, -expenses [firstName] [lastName]\n" +
                    "-ec, -expensesInCategory [firstName] [lastName] [category]\n" +
                    "-v, -visited [country]\n" +
                    "-a, -averageExpenses\n" +
                    "-moa, -theMostOftenAbroad\n" +
                    "-lt, -longestTrip\n" +
                    "-met, -theMostExpensiveTrip");
        } catch (TimeoutException ex) {
            System.out.println("Could not get the response, there may be a network error!");
        } finally {
            system.terminate();
        }

    }

}
