package com.porcupine.utils;

import javaslang.Tuple;
import org.apache.commons.cli.*;

import java.util.Optional;

public class ArgumentParser {

    public static ArgumentResult parse(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(setUpParsersOptions(), args);
        String[] expenses = cmd.getOptionValues("e");
        String[] expensesInCategory = cmd.getOptionValues("ec");

        return new ArgumentResult(
                javaslang.control.Option.of(extractIntegerParam(cmd.getOptionValue("k"))),
                javaslang.control.Option.of(expenses).map((String[] x) -> Tuple.of(x[0], x[1])),
                javaslang.control.Option.of(expensesInCategory).map((String[] x) -> Tuple.of(x[0], x[1], x[2])),
                javaslang.control.Option.of(cmd.getOptionValue("v")),
                cmd.hasOption("a"),
                cmd.hasOption("moa"),
                cmd.hasOption("lt"),
                cmd.hasOption("met"));
    }

    private static Options setUpParsersOptions() {

        Options options = new Options();

        Option termOfOffice =
                Option
                        .builder("k")
                        .longOpt("termOfOffice")
                        .hasArg()
                        .required()
                        .build();

        Option expenses =
                Option
                        .builder("e")
                        .longOpt("expenses")
                        .hasArg()
                        .numberOfArgs(2)
                        .build();

        Option expensesInCategory =
                Option
                        .builder("ec")
                        .longOpt("expensesInCategory")
                        .hasArg()
                        .numberOfArgs(3)
                        .build();

        Option visited =
                Option
                        .builder("v")
                        .longOpt("visited")
                        .hasArg()
                        .numberOfArgs(1)
                        .build();

        options.addOption(termOfOffice)
                .addOption(expenses)
                .addOption(expensesInCategory)
                .addOption(visited)
                .addOption("a", "averageExpenses", false, "average expenses")
                .addOption("lt", "longestTrip", false, "longest trip")
                .addOption("moa", "theMostOftenAbroad", false, "the most often abroad")
                .addOption("met", "theMostExpensiveTrip", false, "the most expensive trip");

        return options;
    }

    private static Integer extractIntegerParam(String arg) throws ParseException {
        try {
            return Optional.ofNullable(arg).map(Integer::valueOf).orElse(null);
        } catch (NumberFormatException ex) {
            throw new ParseException("Could not parse " + arg + " as an integer value");
        }
    }

}

