package com.porcupine.utils;

import com.porcupine.domain.query.QueryRequest;
import com.porcupine.domain.query.actor.ParliamentQueryActor;
import javaslang.Tuple2;
import javaslang.Tuple3;
import javaslang.control.Option;

import java.util.ArrayList;
import java.util.List;

public class ArgumentResult {

    private final Option<Integer> termOfOffice;
    private final Option<Tuple2<String, String>> expenses;
    private final Option<Tuple3<String, String, String>> expensesInCategory;
    private final Option<String> visitedACountry;
    private final boolean averageExpenses;
    private final boolean theMostOftenAbroad;
    private final boolean longestTrip;
    private final boolean theMostExpensiveTrip;

    ArgumentResult(Option<Integer> termOfOffice, Option<Tuple2<String, String>> expenses, Option<Tuple3<String, String, String>> expensesInCategory, Option<String> visitedACountry, boolean averageExpenses, boolean theMostOftenAbroad, boolean longestTrip, boolean theMostExpensiveTrip) {
        this.termOfOffice = termOfOffice;
        this.expenses = expenses;
        this.expensesInCategory = expensesInCategory;
        this.visitedACountry = visitedACountry;
        this.averageExpenses = averageExpenses;
        this.theMostOftenAbroad = theMostOftenAbroad;
        this.longestTrip = longestTrip;
        this.theMostExpensiveTrip = theMostExpensiveTrip;
    }

    public QueryRequest toQueryRequest() {
        String termOfOfficeUrl = termOfOffice.map(x -> "?conditions[poslowie.kadencja]=" + x).getOrElse("");
        List<ParliamentQueryActor.ParliamentCommand> commands = new ArrayList<>();
        if (expenses.isDefined())
            commands.add(expenses.map(x -> new ParliamentQueryActor.TotalExpenses(x._1, x._2)).get());
        if (expensesInCategory.isDefined())
            commands.add(expensesInCategory.map(x -> new ParliamentQueryActor.ExpensesInCategory(x._1, x._2, x._3)).get());
        visitedACountry.forEach(x -> commands.add(new ParliamentQueryActor.VisitedACountry(x)));
        if (averageExpenses) commands.add(new ParliamentQueryActor.AverageExpenses());
        if (longestTrip) commands.add(new ParliamentQueryActor.TheLongestAbroad());
        if (theMostOftenAbroad) commands.add(new ParliamentQueryActor.TheMostOftenAbroad());
        if (theMostExpensiveTrip) commands.add(new ParliamentQueryActor.TheMostExpensiveTrip());

        return new QueryRequest(termOfOfficeUrl, commands);
    }

}
