package com.porcupine.infrastructure.json;

public class JsonServiceResponse<T> {

    public final String firstName;
    public final String lastName;
    public final T resource;

    public JsonServiceResponse(String firstName, String lastName, T resource) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.resource = resource;
    }
}
