package com.porcupine.infrastructure.json;

import com.porcupine.domain.JsonParsingService;
import com.porcupine.domain.query.actor.TheMostOfQueryActor;
import javaslang.Tuple;
import javaslang.Tuple2;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.substringBetween;

@SuppressWarnings("unchecked")
public class JsonParsingServiceImpl implements JsonParsingService {

    public int extractPageNumber(String jsonString) throws JSONException {
        String lastLink = new JSONObject(jsonString).getJSONObject("Links").getString("last");
        String number = substringBetween(lastLink, "page=", "&");
        if (number != null) return Integer.valueOf(number);
        else throw new IllegalStateException("Unexpected pattern of link!");
    }

    public Map<String, String> extractAMapOfNamesToId(String jsonString) throws JSONException {
        return new JSONObject(jsonString)
                .getJSONArray("Dataobject")
                .toList()
                .stream()
                .map(o -> (HashMap) o)
                .collect(Collectors.toMap(this::extractNames, c -> c.get("id").toString()));
    }

    public double extractExpensesSum(String jsonString) throws JSONException {
        return extractYears(extractExpenses(jsonString))
                .map(o -> ((ArrayList) o.get("pola")).stream().mapToDouble(x -> Double.valueOf((String) x)).sum())
                .mapToDouble(i -> i)
                .sum();
    }

    public double extractExpensesInCategory(String category, String jsonString) throws JSONException, IllegalArgumentException {
        JSONObject expenses = extractExpenses(jsonString);
        int index = extractIndex(category, expenses) - 1;
        return extractYears(expenses)
                .map(o -> ((ArrayList) o.get("pola")).get(index))
                .mapToDouble(o -> Double.valueOf(o.toString()))
                .sum();
    }

    public TheMostOfQueryActor.TheMostOfActorResponse<Integer> extractNumberOfTrips(String jsonString) throws JSONException {
        Tuple2<String, String> names = extractNames(jsonString);
        int numberOfTrips = new JSONObject(jsonString).getJSONObject("data").getInt("poslowie.liczba_wyjazdow");
        return new TheMostOfQueryActor.TheMostOfActorResponse<>(names._1, names._2, numberOfTrips);
    }

    public TheMostOfQueryActor.TheMostOfActorResponse<Double> extractMaxTripPrice(String jsonString) throws JSONException {
        Tuple2<String, String> names = extractNames(jsonString);
        Optional<JSONArray> trips = getTrips(jsonString);
        double value = trips
                .map(ts -> ts.toList().stream()
                        .mapToDouble(x -> Double.valueOf((String) ((HashMap) x).get("koszt_suma")))
                        .max()
                        .orElse(0d))
                .orElse(0d);

        return new TheMostOfQueryActor.TheMostOfActorResponse<>(names._1, names._2, value);
    }

    public TheMostOfQueryActor.TheMostOfActorResponse<Integer> extractTotalTripLength(String jsonString) throws JSONException {
        Tuple2<String, String> names = extractNames(jsonString);
        Optional<JSONArray> trips = getTrips(jsonString);
        int totalLength = trips.map(ts -> ts.toList().stream().mapToInt(this::toPeriodLength).sum()).orElse(0);
        return new TheMostOfQueryActor.TheMostOfActorResponse<>(names._1, names._2, totalLength);
    }

    public JsonServiceResponse<List<String>> extractVisitedCountries(String jsonString) throws JSONException {
        Tuple2<String, String> names = extractNames(jsonString);
        Optional<JSONArray> trips = getTrips(jsonString);
        return new JsonServiceResponse<>(names._1, names._2, trips.map(this::extractListOfCountries).orElse(new ArrayList<>()));
    }


    /* helper methods */

    private String extractNames(HashMap map) throws JSONException {
        HashMap data = (HashMap) map.get("data");
        return data.get("poslowie.imie_pierwsze").toString() + " " + data.get("poslowie.nazwisko").toString();
    }

    private JSONObject extractExpenses(String jsonString) throws JSONException {
        return new JSONObject(jsonString)
                .getJSONObject("layers")
                .getJSONObject("wydatki");
    }

    private Stream<HashMap> extractYears(JSONObject expenses) throws JSONException {
        return expenses
                .getJSONArray("roczniki")
                .toList()
                .stream()
                .map(o -> (HashMap) o);
    }

    private Integer extractIndex(String category, JSONObject expenses) throws JSONException, IllegalArgumentException {
        return expenses
                .getJSONArray("punkty")
                .toList()
                .stream()
                .map(o -> ((HashMap) o))
                .filter(o -> o.get("tytul").toString().equals(category))
                .map(o -> Integer.valueOf(o.get("numer").toString()))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private Optional<JSONArray> getTrips(String jsonString) throws JSONException {
        return Optional.ofNullable(new JSONObject(jsonString).getJSONObject("layers").optJSONArray("wyjazdy"));
    }

    private Integer toPeriodLength(Object tripObject) throws JSONException {
        HashMap trip = (HashMap) tripObject;
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime from = formatter.parseDateTime((String) trip.get("od"));
        DateTime to = formatter.parseDateTime((String) trip.get("do"));
        return Days.daysBetween(from, to).getDays();
    }

    private Tuple2<String, String> extractNames(String jsonString) throws JSONException {
        JSONObject data = new JSONObject(jsonString).getJSONObject("data");
        String firstName = data.getString("poslowie.imie_pierwsze");
        String lastName = data.getString("poslowie.nazwisko");
        return Tuple.of(firstName, lastName);
    }

    private List<String> extractListOfCountries(JSONArray trips) throws JSONException {
        return trips.toList().stream().map(trip -> (String) ((HashMap) trip).get("kraj")).collect(Collectors.toList());
    }

}