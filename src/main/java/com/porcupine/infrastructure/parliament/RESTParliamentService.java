package com.porcupine.infrastructure.parliament;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.Materializer;
import com.porcupine.domain.ParliamentService;

import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class RESTParliamentService implements ParliamentService {

    private final ActorSystem system;
    private final Materializer materializer;

    private final String domain;

    public RESTParliamentService(ActorSystem system, Materializer materializer, String domain) {
        this.system = system;
        this.materializer = materializer;
        this.domain = domain;
    }

    public <T> CompletionStage<T> consumeRequest(String url, Function<String, T> consumer) {
        return getResource(domain + url)
                .thenCompose(httpResponse -> httpResponse.entity().toStrict(10000, materializer))
                .thenApply(entity -> entity.getData().decodeString("UTF-8"))
                .thenApply(consumer);
    }

    private CompletionStage<HttpResponse> getResource(String url) {
        return Http.get(system).singleRequest(HttpRequest.create(url), materializer);
    }

}
